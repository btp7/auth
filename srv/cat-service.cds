using my.bookshop as my from '../db/data-model';


service CatalogService @(requires: 'authenticated-user'){

    @readonly entity Books @(restrict: [{grant: 'READ', to: 'catalogread' }]) as projection on my.Books;
}

service CatalogService2 @(requires: 'authenticated-user'){
    @readonly entity Books as projection on my.Books;
}
